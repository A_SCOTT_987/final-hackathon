import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradesHistoryComponent } from './trades-history.component';

describe('TradesHistoryComponent', () => {
  let component: TradesHistoryComponent;
  let fixture: ComponentFixture<TradesHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradesHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradesHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
