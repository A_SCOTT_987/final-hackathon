import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trades } from '../_models/trades';
import { Observable, Subject, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TradesService {

  url = 'http://hackathon-hackathon.emeadocker36.conygre.com/api/v1/trades/';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getTrades(): Observable<Trades> {
    return this.http.get<Trades>(this.url + "findAll")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getTrade(id:any): Observable<Trades> {
    return this.http.get<Trades>(this.url + "/findId/" + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  createTrade(trade:Trades): Observable<Trades> {
    console.log('I am here');
    return this.http.post<Trades>(this.url + "create", JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  updateTrade(id:number, trade:Trades): Observable<Trades> {
    return this.http.put<Trades>(this.url + "/update" + '', JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  deleteTrade(id:number){
    return this.http.delete<Trades>(this.url + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

