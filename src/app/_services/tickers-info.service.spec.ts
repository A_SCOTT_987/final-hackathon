import { TestBed } from '@angular/core/testing';

import { TickersInfoService } from './tickers-info.service';

describe('TickersInfoService', () => {
  let service: TickersInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TickersInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
