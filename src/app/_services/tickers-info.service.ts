import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { Tickers } from '../_models/tickers';

@Injectable({
  providedIn: 'root'
})
export class TickersInfoService {

  url = 'https://v588nmxc10.execute-api.us-east-1.amazonaws.com/default/tickerList';

  priceUrl = 'https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2'

  constructor(private http: HttpClient) { }

  getTickers(): Observable<Tickers> {
    return this.http.get<Tickers>(this.url)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getTickerPrice(ticker: string) {
    let params = new HttpParams().set('ticker', ticker).set('num_days', '1');  
    return this.http.get(this.priceUrl, {params: params})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
