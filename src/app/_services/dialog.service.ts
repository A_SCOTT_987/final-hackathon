import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PlaceOrderDialogComponent } from '../place-order-dialog/place-order-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  volume: number;

  constructor(public dialog: MatDialog) { }

  openDialogOrder(ticker: any, price: any) {
    const dialogRef = this.dialog.open(PlaceOrderDialogComponent, {
      width: '300px',
      height: '300px',
      data: {ticker: ticker, price: price, volume: this.volume}
    });
    return dialogRef.afterClosed();
  }
}
