export class Trades {

  constructor(){
    this.id=0;
    this.buyOrSell="";
    this.price=0;
    this.statusCode=0;
    this.stockTicker="";
    this.volume=0;
    this.tradeType="";
    this.portfolioID=1;
  }
  
  id: number;
  buyOrSell: string;
  price: number;
  statusCode: number;
  stockTicker: string;
  volume: number;
  tradeType: string;
  portfolioID: number;
}
