import { NgModule } from '@angular/core';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradesCreateComponent } from './trades-create/trades-create.component';
import { TradesEditComponent } from './trades-edit/trades-edit.component';
import { TradesListComponent } from './trades-list/trades-list.component';
import { HomeComponent } from './home/home.component';
import { SplashScreenComponent } from './splash-screen/splash-screen.component';
import { NavComponent } from './nav/nav.component';
import { TickersListComponent } from './tickers-list/tickers-list.component';
import { TradesHistoryComponent } from './trades-history/trades-history.component';
import { StatusCodeHistoryComponent } from './status-code-history/status-code-history.component';
import { PlaceOrderDialogComponent } from './place-order-dialog/place-order-dialog.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatChipsModule } from "@angular/material/chips";
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSliderModule} from '@angular/material/slider';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import { OrderSummaryComponent } from './order-summary/order-summary.component';

@NgModule({
  declarations: [
    AppComponent,
    TradesCreateComponent,
    TradesEditComponent,
    TradesListComponent,
    HomeComponent,
    SplashScreenComponent,
    NavComponent,
    TickersListComponent,
    TradesHistoryComponent,
    StatusCodeHistoryComponent,
    PlaceOrderDialogComponent,
    OrderSummaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSliderModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    HammerModule
  ],
  providers: [StatusCodeHistoryComponent, OrderSummaryComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
