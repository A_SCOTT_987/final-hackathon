import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradesCreateComponent } from './trades-create.component';

describe('TradesCreateComponent', () => {
  let component: TradesCreateComponent;
  let fixture: ComponentFixture<TradesCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradesCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
