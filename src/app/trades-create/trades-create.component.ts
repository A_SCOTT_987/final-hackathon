import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { TradesService } from '../_services/trades.service';

@Component({
  selector: 'app-trades-create',
  templateUrl: './trades-create.component.html',
  styleUrls: ['./trades-create.component.scss']
})
export class TradesCreateComponent implements OnInit {

  @Input() tradeDetails = { id: 0, buyOrSell: '', price: 0, statusCode: 0, stockTicker: '', volume: 0, createdTimestamp: '' }

  constructor(
    public tradesService: TradesService,
    public router: Router) { }

  ngOnInit(): void {
  }

  addTrade() {
    // this.tradesService.createTrade(this.tradeDetails).subscribe((data: {}) => {
    //   this.router.navigate(['/shipper-list'])
    // })
  }
}
