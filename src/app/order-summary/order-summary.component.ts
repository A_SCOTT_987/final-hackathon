import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Trades } from '../_models/trades';
import { TickersInfoService } from '../_services/tickers-info.service';
import { TradesService } from '../_services/trades.service';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit {

  orderResponse: Observable<any>;

  constructor(public tradesService: TradesService) { }

  ngOnInit(): void {
  }

  setObservable() {

  }

}
