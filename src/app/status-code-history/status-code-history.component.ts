import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-status-code-history',
  templateUrl: './status-code-history.component.html',
  styleUrls: ['./status-code-history.component.scss']
})
export class StatusCodeHistoryComponent implements OnInit {

  statusCode: number;

  constructor() { }

  ngOnInit(): void {
    console.log(this.statusCode);
  }

}
