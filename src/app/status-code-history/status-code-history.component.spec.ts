import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusCodeHistoryComponent } from './status-code-history.component';

describe('StatusCodeHistoryComponent', () => {
  let component: StatusCodeHistoryComponent;
  let fixture: ComponentFixture<StatusCodeHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatusCodeHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusCodeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
