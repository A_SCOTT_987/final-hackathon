import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradesEditComponent } from './trades-edit.component';

describe('TradesEditComponent', () => {
  let component: TradesEditComponent;
  let fixture: ComponentFixture<TradesEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradesEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
