import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TradesService } from '../_services/trades.service';

@Component({
  selector: 'app-trades-edit',
  templateUrl: './trades-edit.component.html',
  styleUrls: ['./trades-edit.component.scss']
})
export class TradesEditComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  tradeDetails: any = {};

  constructor(
    public tradesService: TradesService,
    public actRoute: ActivatedRoute,
    public router: Router) { }

  ngOnInit(): void {
    this.tradesService.getTrade(this.id).subscribe((data: {}) => {
      this.tradeDetails = data;
      })
    }

  updateTrade() {
    if(window.confirm('Are you sure, you want to update?')){
      this.tradesService.updateTrade(this.id, this.tradeDetails).subscribe(data => {
        this.router.navigate(['/trades-list'])
      })
    }
  }
}
