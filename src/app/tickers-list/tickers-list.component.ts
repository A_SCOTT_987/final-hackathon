import { Component, Input, OnInit } from '@angular/core';
import { TickersInfoService } from '../_services/tickers-info.service';
import * as tickerList from '../../assets/ticker-list.json'
import { DialogService } from '../_services/dialog.service';
import { TradesService } from '../_services/trades.service';
import { Trades } from '../_models/trades';
import { OrderSummaryComponent } from '../order-summary/order-summary.component';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-tickers-list',
  templateUrl: './tickers-list.component.html',
  styleUrls: ['./tickers-list.component.scss']
})
export class TickersListComponent implements OnInit {


  tickers: any = [];
  orderDetails = { id: 0, buyOrSell: '', price: 0, statusCode: 0, stockTicker: '', portfolioID: 1, volume: 0, tradeType:''}

  constructor(private tickersInfoService: TickersInfoService,
              public dialogService: DialogService,
              private tradesService: TradesService,
              public orderSummary: OrderSummaryComponent) { }

  ngOnInit(): void {
    this.loadTickers();
  }

  loadTickers() {
    this.tickersInfoService.getTickers().subscribe((data: {}) => {
      this.tickers = data;
      console.log(this.tickers);
    });

  }

  openDialog(ticker: string)  {
    this.tickersInfoService.getTickerPrice(ticker).subscribe(
      (data: any) => {
        console.log(data.price_data[0].value);
        let price = data.price_data[0].value.toFixed(4);
        this.dialogService.openDialogOrder(ticker, price).subscribe(
          (data) => {
            console.log(data);
            this.orderDetails.buyOrSell = 'BUY';
            this.orderDetails.stockTicker = data.ticker;
            this.orderDetails.price = data.price;
            this.orderDetails.volume = data.volume;
            this.orderDetails.tradeType = 'Stock'
            console.log(this.orderDetails);
            this.tradesService.createTrade(this.orderDetails).subscribe(
              (data) => {
                console.log(data);
                this.orderSummary.orderResponse = of(data);
              }
            );
          }
        );
      }
    );
  }
}
