import { Component, OnInit } from '@angular/core';
import { StatusCodeHistoryComponent } from '../status-code-history/status-code-history.component';
import { TradesService } from '../_services/trades.service';

@Component({
  selector: 'app-trades-list',
  templateUrl: './trades-list.component.html',
  styleUrls: ['./trades-list.component.scss']
})
export class TradesListComponent implements OnInit {
  trades: any = [];

  constructor(public tradesService: TradesService,
              public statusCodeHistoryComponent: StatusCodeHistoryComponent) { }

  ngOnInit(): void {
    this.loadTrades()
  }

  loadTrades() {
    return this.tradesService.getTrades().subscribe((data: {}) => {
      this.trades = data;
      this.trades.forEach((trade: any) => {
        var jsonDate = trade.createdTimestamp;
        var date = new Date(jsonDate);
        console.log(date.toLocaleDateString("en-US"));
        trade.createdTimestamp = date.toLocaleDateString("en-US");
      });
    });
    
  }

  deleteTrades(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.tradesService.deleteTrade(id).subscribe(data => {
        this.loadTrades()
      })
    }
  }

  getStatusCodeHistory(statusCode: number) {
    this.statusCodeHistoryComponent.statusCode = statusCode;
    this.statusCodeHistoryComponent.ngOnInit();
  }

}
