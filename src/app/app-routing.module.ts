import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SplashScreenComponent } from './splash-screen/splash-screen.component';
import { TradesCreateComponent } from './trades-create/trades-create.component';
import { TradesEditComponent } from './trades-edit/trades-edit.component';
import { TradesHistoryComponent } from './trades-history/trades-history.component';
import { TradesListComponent } from './trades-list/trades-list.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'splash' },
  { path: 'splash', component: SplashScreenComponent },
  { path: 'home', component: HomeComponent },
  { path: 'trades-history', component: TradesHistoryComponent },
  { path: 'trades-create', component: TradesCreateComponent },
  { path: 'trades-list', component: TradesListComponent },
  { path: 'trades-edit/:id', component: TradesEditComponent },
  { path: '**', component: TradesListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
